def bubbleSort(arr):
   n = len(arr)
   # Traverse through all array elements
   for x in range(n):
   # The Last x elements are already in correct position
    for y in range(0, n-x-1):
      # Swap if the element found is greater than the next element
      if arr[y] > arr[y+1] :
         arr[y], arr[y+1] = arr[y+1], arr[y]
# Driver code to test above
arr = ['g','u','s','t','i','a','r','s','y','a','d']
bubbleSort(arr)
#print ("SORTED ARRAY IS:")
for z in range(len(arr)):
   print (arr[z])

import time
starttime = time.time()
#long running
for i in range(3):
    time.sleep(0.1)
    
endtime = time.time()
t = endtime - starttime
print(t)