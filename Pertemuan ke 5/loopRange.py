#Fungsi Rentang()
#Untuk mengulang satu set kode beberapa kali, 
#kita dapat menggunakan fungsi range(),
#Fungsi range() mengembalikan urutan angka, 
#mulai dari 0 secara default, dan bertambah 1 
#(secara default), dan berakhir pada angka yang ditentukan.

# for xyz in range(5): # 0, 1, 2, 3, 4
#     print(xyz)

#Perhatikan bahwa range(5) bukanlah nilai 0 hingga 5, 
# tetapi nilai 0 hingga 4.

# for xyz in range(3, 6): # 3, 4, 5
#      print(xyz)
#
for x in range(3, 30, 4): #4 ditambah 3 sampai nilai sebelum 30
  print(x)

