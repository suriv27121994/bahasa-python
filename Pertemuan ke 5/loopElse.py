#Cetak semua angka dari 0 hingga 4, dan cetak pesan saat loop telah berakhir:

for x in range(5):
  print(x)
else:
  print("Finally finished!") 

#Catatan: Blok else TIDAK akan dieksekusi jika loop dihentikan oleh pernyataan break.