#An example loop using while
#Catatan: Penggunaan modulo pada 
#kondisional mengasumsikan bahwa nilai  
#nol sebagai False(salah) dan selain itu True 1 (benar)

i = 4
while(i < 100):
    j = 2
    while(j <= (i/j)):
        if not(i%j): break
        j = j + 1
    if (j > i/j) : print(i, " is prime")
    i = i + 1

print("Good byeeee!")