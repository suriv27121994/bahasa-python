#Pernyataan lanjutan
#Dengan pernyataan continue kita dapat menghentikan 
#iterasi loop saat ini, dan melanjutkan dengan yang berikutnya:
varFruits = ["Banana", "Mango", "Cherry"]
for x in varFruits:
    
    if x == "Mango":
        continue
    print(x)
