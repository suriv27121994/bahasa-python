varFruits = ["Banana", "Apple", "Mango", "Cherry"]
#Pernyataan break
#Dengan pernyataan break kita dapat menghentikan 
#loop sebelum loop melewati semua item:

# for x in varFruits:
#     print(x) #menampilkan hasil nilai x
#     if x == "Cherry":
#         break

#Keluar dari loop ketika x adalah "apel", 
#tetapi kali ini jeda muncul sebelum cetakan:
for x in varFruits:
    print(x)
    if x == "Apple":
        break
    #print(x)