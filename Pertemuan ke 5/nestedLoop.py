#Loop bersarang adalah loop di dalam loop.
#"Loop dalam" akan dieksekusi satu kali untuk 
# setiap iterasi dari "loop luar":
color = ["Biru", "Merah", "Hitam"]
fruits = ["Banana", "Mango", "Apple"]

for x in color:
  for y in fruits:
    print(x, y) #cetak x dan y