list1 = ["apple", "banana", "cherry"] #string (teks)
list2 = [1, 5, 7, 9, 3] # integer
list3 = [True, False, False] #boolean True (1) dan false (0)

listCampuran = ["abc", 34, True, 40, "male"]

# print(list1)
# print(list2)
# print(list3)
print(listCampuran)